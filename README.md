# Study Case: Shopping Cart Microservice 

## Premises
 
- Shop type: B2B
- Source of Product data: Sync (Event listener? shared DB? Endpoint?)
- Source of Stock Data: Sync
- Use OpenAPI definition to guarantee
- Frontend application takes care of user authentication, product info etc.

## Scope
- 4 endpoints for CRUD operations
- It's inferred that more endpoints are needed: list running orders, delete order
- Stateless Microservice, except for DB persistence
  - prevent slowdowns from accessing external systems 
- Authentication: external service (OAuth2, for example) validating credentials (not implemented)
- Persistence: own DB for  of data, with sync through Message Broker to main DB
  - Best case: use Event Storage data model, to preserve log and keep all transactions immutable (not implemented here)

## Implementation details:
- Choice of technology:
  - Given the short time and being it my current daily programming platform:
    - Language: PHP (will discuss better alternatives on meeting)
    - Framework: Slim (microframework for REST services)
    - Persistence: MySQL (will discuss better alternatives on meeting)
    - Deployment: docker container (not implemented, due to time)
- Use of OpenAPI Generator to create base server skeleton from single OAD file

