<?php

use Case\ShoppingCartMS\App\RegisterDependencies;
use DI\ContainerBuilder;

require_once __DIR__ . '/vendor/autoload.php';

// Instantiate PHP-DI ContainerBuilder
$builder = new ContainerBuilder();

// consider prod by default
$env = match (strtolower($_SERVER['APP_ENV'] ?? 'prod')) {
    'development', 'dev' => 'dev',
    default => 'prod',
};

// Main configuration
$builder->addDefinitions(__DIR__ . "/config/{$env}/default.inc.php");

// Config file for the environment if exists
$userConfig = __DIR__ . "/config/{$env}/config.inc.php";
if (file_exists($userConfig)) {
    $builder->addDefinitions($userConfig);
}

// Set up dependencies
$dependencies = new RegisterDependencies();
$dependencies($builder);

// Build PHP-DI Container instance
try {
    return $builder->build();
} catch (Throwable $throwable) {
    http_response_code(500);
    exit();
}
