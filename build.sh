#!/usr/bin/env bash

# Script for generating the

set -e

docker pull openapitools/openapi-generator-cli
docker run \
	--user $(id -u):$(id -g) \
	--rm \
	-v ${PWD}:/local \
	openapitools/openapi-generator-cli \
	generate \
	-i https://gitlab.com/thomasphansen/shopping-cart-api/-/raw/main/ShoppingCart.yaml \
	-g php-slim4 \
	-o /local \
	-c /local/config.yaml
