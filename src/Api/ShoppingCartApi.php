<?php

namespace Case\ShoppingCartMS\Api;

use Case\ShoppingCartMS\Domain\Customer;
use Case\ShoppingCartMS\Domain\Order;
use Case\ShoppingCartMS\Domain\OrderLine;
use Case\ShoppingCartMS\Model\AddItemRequest;
use Case\ShoppingCartMS\Model\ChangeQuantityItemWrite;
use Case\ShoppingCartMS\Model\ChangeQuantityRequest;
use Case\ShoppingCartMS\Model\ItemWrite;
use Doctrine\ORM\EntityManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpInternalServerErrorException;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpSpecializedException;
use Throwable;

class ShoppingCartApi extends AbstractShoppingCartApi
{
    public function __construct(private readonly EntityManager $entityManager)
    {
    }

    public function listItems(ServerRequestInterface $request, ResponseInterface $response, string $orderId): ResponseInterface
    {
        try {
            /** @var Customer $customer */
            $customer = $request->getAttribute('authenticated_user');

            /** @var Order $order */
            $order = $this->entityManager->getRepository(Order::class)
                ->findOneBy([
                    'customerId' => $customer->getId(),
                    'uuid' => $orderId
                ])
            ;
            if ($order === null) {
                throw new HttpNotFoundException($request);
            }

            $orderLines = $this->entityManager->getRepository(OrderLine::class)
                ->findBy([
                    'order' => $order
                ])
            ;

            $payload = [
                'data' => $orderLines ?? []
            ];

            $response->getBody()->write(json_encode($payload, JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT));

            return $response->withHeader('Content-Type', 'application/json');
        } catch (HttpSpecializedException $exception) {
            throw $exception;
        } catch (Throwable $throwable) {
            throw new HttpInternalServerErrorException($request, null, $throwable);
        }
    }

    public function addItem(ServerRequestInterface $request, ResponseInterface $response, string $orderId): ResponseInterface
    {
        /** @var Customer $customer */
        $customer = $request->getAttribute('authenticated_user');

        $body = $request->getParsedBody();

        $this->entityManager->beginTransaction();
        try {
            if ($orderId === '0') {
                $order = Order::createNew($customer->getId());
                $this->entityManager->persist($order);
                $this->entityManager->flush();
            } else {
                $order = $this->entityManager->getRepository(Order::class)
                    ->findOneBy([
                        'customerId' => $customer->getId(),
                        'uuid' => $orderId
                    ])
                ;

                if ($order === null) {
                    throw new HttpNotFoundException($request);
                }
            }

            /** @var AddItemRequest $addItemRequest */
            $addItemRequest = AddItemRequest::createFromData($body);
            /** @var ItemWrite $oItem */
            $oItem = ItemWrite::createFromData($addItemRequest->data);

            #TODO: Validate product GTIN against local DB or external service

            $orderLine = OrderLine::createNew($order, $oItem);
            $this->entityManager->persist($orderLine);
            $this->entityManager->flush();

            $payload = [
                'data' => [
                    'orderId' => $order->getUUID(),
                    'item' => $orderLine
                ]
            ];

            $response->getBody()->write(json_encode($payload, JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT));
            $this->entityManager->commit();

            # TODO: Create Event to adjust stock

            return $response->withHeader('Content-Type', 'application/json');
        } catch (HttpSpecializedException $exception) {
            throw $exception;
        } catch (Throwable $throwable) {
            $this->entityManager->rollback();
            throw new HttpInternalServerErrorException($request, null, $throwable);
        }
    }

    public function changeQuantity(ServerRequestInterface $request, ResponseInterface $response, string $orderId, string $itemId): ResponseInterface
    {
        $this->entityManager->beginTransaction();
        try {
            $orderLine = $this->getOrderLine($request, $orderId, $itemId);

            $body = $request->getParsedBody();

            /** @var ChangeQuantityRequest $addItemRequest */
            $addItemRequest = ChangeQuantityRequest::createFromData($body);
            /** @var ChangeQuantityItemWrite $oItem */
            $oItem = ChangeQuantityItemWrite::createFromData($addItemRequest->data);

            $orderLine->setQuantity($oItem->quantity);
            $this->entityManager->persist($orderLine);
            $this->entityManager->flush();

            $payload = [
                'data' => [
                    'item' => $orderLine
                ]
            ];

            $response->getBody()->write(json_encode($payload, JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT));
            $this->entityManager->commit();

            # TODO: Create Event to adjust stock

            return $response->withHeader('Content-Type', 'application/json');
        } catch (HttpSpecializedException $exception) {
            throw $exception;
        } catch (Throwable $throwable) {
            $this->entityManager->rollback();
            throw new HttpInternalServerErrorException($request, null, $throwable);
        }
    }

    public function getItem(ServerRequestInterface $request, ResponseInterface $response, string $orderId, string $itemId): ResponseInterface
    {
        try {
            $orderLine = $this->getOrderLine($request, $orderId, $itemId);

            $payload = [
                'data' => [
                    'item' => $orderLine
                ]
            ];

            $response->getBody()->write(json_encode($payload, JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT));

            return $response->withHeader('Content-Type', 'application/json');
        } catch (HttpSpecializedException $exception) {
            throw $exception;
        } catch (Throwable $throwable) {
            throw new HttpInternalServerErrorException($request, null, $throwable);
        }
    }

    public function deleteItem(ServerRequestInterface $request, ResponseInterface $response, string $orderId, string $itemId): ResponseInterface
    {
        $this->entityManager->beginTransaction();
        try {
            $orderLine = $this->getOrderLine($request, $orderId, $itemId);

            $this->entityManager->remove($orderLine);
            $this->entityManager->flush();
            $this->entityManager->commit();

            # TODO: Create Event to adjust stock

            return $response->withHeader('Content-Type', 'application/json')->withStatus(204);
        } catch (HttpSpecializedException $exception) {
            throw $exception;
        } catch (Throwable $throwable) {
            $this->entityManager->rollback();
            throw new HttpInternalServerErrorException($request, null, $throwable);
        }
    }

    private function getOrderLine(ServerRequestInterface $request, string $orderId, string $itemId): OrderLine
    {
        /** @var Customer $customer */
        $customer = $request->getAttribute('authenticated_user');

        /** @var Order $order */
        $order = $this->entityManager->getRepository(Order::class)
            ->findOneBy([
                'customerId' => $customer->getId(),
                'uuid' => $orderId
            ])
        ;
        if ($order === null) {
            throw new HttpNotFoundException($request);
        }

        /** @var OrderLine $orderLine */
        $orderLine = $this->entityManager->getRepository(OrderLine::class)
            ->findOneBy([
                'order' => $order,
                'uuid' => $itemId,
            ])
        ;

        if ($orderLine === null) {
            throw new HttpNotFoundException($request);
        }

        return $orderLine;
    }
}
