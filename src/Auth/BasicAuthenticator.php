<?php

namespace Case\ShoppingCartMS\Auth;

use Case\ShoppingCartMS\Domain\Customer;

class BasicAuthenticator extends AbstractAuthenticator
{

    protected function getUserByToken(string $token)
    {
        # TODO: access external Authorization/Authentication system, which will provide the Customer data.
        return new Customer(1);
    }
}