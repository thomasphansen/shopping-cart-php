<?php

namespace Case\ShoppingCartMS\Domain;

use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OrderBy;
use Doctrine\ORM\Mapping\Table;
use Ramsey\Uuid\Uuid;

#[Entity, Table(name: 'orders')]
final class Order
{
    #[Id, Column(type: 'integer'), GeneratedValue(strategy: 'AUTO')]
    private int $id;

    #[Column(type: 'string', length: 36, nullable: false)]
    private string $uuid;

    #[Column(name: 'customer_id', type: 'integer', nullable: false)]
    private int $customerId;

    #[Column(name: 'added_date', type: 'datetimetz_immutable', nullable: false)]
    private DateTimeImmutable $addedDate;

    /**
     * @var Collection<int, OrderLine>
     */
    #[OneToMany(mappedBy: 'order_id', targetEntity: OrderLine::class, orphanRemoval: true)]
    #[OrderBy(['id' => 'ASC'])]
    private Collection $orderLines;

    public function __construct()
    {
        $this->orderLines = new ArrayCollection();
    }

    public static function createNew(int $customerId): self
    {
        $order = new self();
        $order->customerId = $customerId;
        $order->addedDate = new DateTimeImmutable();
        $order->uuid = Uuid::uuid4()->toString();

        return $order;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getUUID(): string
    {
        return $this->uuid;
    }

    public function getCustomerId(): int
    {
        return $this->customerId;
    }

    public function getAddedDate(): DateTimeImmutable
    {
        return $this->addedDate;
    }

    /**
     * @return Collection|OrderLine[]
     */
    public function getOrderLines(): Collection|array
    {
        return $this->orderLines;
    }
}