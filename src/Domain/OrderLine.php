<?php

namespace Case\ShoppingCartMS\Domain;

use Case\ShoppingCartMS\Model\ItemWrite;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use JsonSerializable;
use Ramsey\Uuid\Uuid;

#[Entity, Table(name: 'order_lines')]
final class OrderLine implements JsonSerializable
{
    #[Id, Column(type: 'integer'), GeneratedValue(strategy: 'AUTO')]
    private ?int $id = null;

    #[Column(type: 'string', length: 36, nullable: false)]
    private ?string $uuid = null;

    #[ManyToOne(inversedBy: 'orderLines')]
    #[JoinColumn(name: 'order_id', nullable: false)]
    private ?Order $order = null;

    #[Column(type: 'string', length: '18', nullable: false)]
    private ?string $gtin = null;

    #[Column(type: 'integer', nullable: false)]
    private ?int $quantity = null;

    #[Column(name: 'unit_price', type: 'float', nullable: false)]
    private ?float $unitPrice = null;

    #[Column(type: 'string', nullable: false)]
    private ?string $currency = null;

    #[Column(name: 'added_date', type: 'datetimetz_immutable', nullable: false)]
    private ?DateTimeImmutable $addedDate = null;

    public static function createNew(Order $order, ItemWrite $oItem): self
    {
        $orderLine = new self();
        $orderLine->uuid = Uuid::uuid4()->toString();
        $orderLine->order = $order;
        $orderLine->gtin = $oItem->gtin;
        $orderLine->unitPrice = $oItem->unitPrice;
        $orderLine->quantity = $oItem->quantity;
        $orderLine->currency = $oItem->currency;
        $orderLine->addedDate = new DateTimeImmutable();

        return $orderLine;
    }


    public function getId(): int
    {
        return $this->id;
    }

    public function getUUID(): string
    {
        return $this->uuid;
    }

    public function getGTIN(): string
    {
        return $this->gtin;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }

    public function getUnitPrice(): float
    {
        return $this->unitPrice;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function getTotalPrice(): float
    {
        return $this->unitPrice * $this->quantity;
    }

    public function getAddedDate(): DateTimeImmutable
    {
        return $this->addedDate;
    }

    public function getLink(): string
    {
        return "/v1/order/{$this->order->getUUID()}/item/{$this->getUUID()}";
    }

    public function jsonSerialize(): array
    {
        return [
            'link' => $this->getLink(),
            'id' => $this->getUUID(),
            'orderId' => $this->order->getUUID(),
            'gtin' => $this->getGTIN(),
            'quantity' => $this->getQuantity(),
            'unitPrice' => $this->getUnitPrice(),
            'totalPrice' => $this->getTotalPrice(),
            'currency' => $this->getCurrency(),
            'datetime' => $this->getAddedDate()->format(DateTimeInterface::ATOM)
        ];
    }
}